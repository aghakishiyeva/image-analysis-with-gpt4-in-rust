# Image Analysis Tool with GPT-4 in Rust

**Team Number:** 20  
**Team Members:** Gunel Aghakishiyeva, Ayush Gupta 

## Overview
In this final project for IDS721 - Data Analysis at Scale in Cloud, we have developed a tool that describes any publicly accessible image provided via a link. This tool is implemented in Rust, utilizing [OpenAI Dive](https://github.com/tjardoo/openai-client/), an unofficial asynchronous Rust library designed to interact with the OpenAI API.

## Main Files

- `src/main.rs`: Contains the core logic of the application.
- `static/index.html`: Provides the user interface for the tool.
- `.env`: Stores the API key required for interacting with the OpenAI GPT-4 model.

## Setup and Usage

1. Clone the repository:

```
git clone https://gitlab.com/aghakishiyeva/image-analysis-with-gpt4-in-rust
```

2. Navigate to the project directory:

```
cd image-analysis-with-gpt4-in-rust
```

3. Update the `.env` file with your OpenAI API key.

4. Start the application:

```
cargo run
```

5. Access the tool via a web browser:

```
http://127.0.0.1:8089
```


6. Enter the URL of a publicly accessible image and click `Analyze`.


![Duke Chapel Picture Analysis](https://i.ibb.co/fXdmg6r/Screenshot-2024-04-27-at-11-42-03-PM.png)

## Deployment

### Containerizing and Deploying Rust Web Service to Kubernetes
Here are step-by-step instructions on how to containerize a Rust web service using Docker and deploy it to a Kubernetes cluster. 

#### Prerequisites
1. Rust installed on your development machine
2. Docker installed on your development machine
3. Kubernetes cluster set up and accessible (e.g.Desktop with Kubernetes enabled)

#### Steps 
1. Containerize the Rust Web Service by writing a `Dockerfile`. 
2. Build the Docker image. 
`
```bash
docker build -t your_image_name:tag .
```
3. Deploy to Kubernetes
```bash
docker login
docker push your_image_name:tag
```
4. Create Kubernetes deployment YAML file (deployment.yaml)

5. Apply the deployment YAML file to create the deployment:

```bash
kubectl apply -f deployment.yaml
```
![Alt text](images/kubernotes_deployment.png)

6. Expose the deployment as a Kubernetes service.

```bash
kubectl expose deployment your-deployment --type=LoadBalancer --port=8085 --target-port=8085
```

![Alt text](images/labels_kubernotes.png)

## Scaling 
Scalability is crucial for ensuring that our application can handle increased traffic and workload without compromising performance or stability. Kubernetes provides mechanisms for automatically scaling our application based on demand.

We utilize Horizontal Pod Autoscaling (HPA) in Kubernetes to automatically adjust the number of pods in our deployment based on CPU utilization or other custom metrics.

![Alt text](images/scaling.png)

## Load Testing 
To ensure the reliability and performance of our application under various levels of load, we conduct load testing. Load testing helps us understand how our application behaves when subjected to a high volume of concurrent users.
The results demonstrated that the application can effectively handle up to 10,000 concurrent users. The maximum response time recorded during this load test was 70,000 milliseconds (or 70 seconds). 

![Alt text](images/Load_Test_v1.png)


## CI/CD 
We used Gitlab CI/CD pipelines to automate the building, testing, and deploying of our application :

Build: Code changes are compiled and artifacts generated automatically.
Test: Automated tests ensure code quality and provide immediate feedback.
Deploy: Passing changes are automatically deployed, minimizing risk and enabling quick rollbacks.

![Alt text](images/CICD.png)



## Additional Information

- **Demo Video**: See the `demovideo.mov`.
- **Kubernetes Deployment**: See the `Images` folder.
- **CI/CD Pipeline**: Documentation available in the `Images` folder or directly in the repository's CI/CD settings.


