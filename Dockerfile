# Use a Rust base image
FROM rust:latest as builder

WORKDIR /usr/src/app

# Copy the Cargo.toml and Cargo.lock files to optimize dependencies
COPY Cargo.toml Cargo.lock ./

# Build dependencies
RUN mkdir src && echo "fn main() {println!(\"dummy\")}" > src/main.rs
RUN cargo build --release
RUN rm -f target/release/deps/openai_dive*

# Copy the source code
COPY . .

# Build the application
RUN cargo build --release

# Final image
FROM debian:buster-slim

WORKDIR /usr/src/app

# Copy the built binary from the builder image
COPY --from=builder /usr/src/app/target/release/finalproject .

CMD ["./finalproject"]
