use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use dotenv::dotenv;
use openai_dive::v1::api::Client;
use openai_dive::v1::models::Gpt4Engine;
use openai_dive::v1::resources::chat::{ChatCompletionParameters, ChatMessage, ChatMessageContent, ImageUrl, ImageUrlType, Role};
use serde::Deserialize;
use std::env;

#[derive(Deserialize)]
struct ImageQuery {
    url: String,
}

async fn analyze_image(query: web::Query<ImageQuery>, client: web::Data<Client>) -> impl Responder {
    let url = query.url.clone();
    let parameters = ChatCompletionParameters {
        model: Gpt4Engine::Gpt4VisionPreview.to_string(),
        messages: vec![
            ChatMessage {
                role: Role::User,
                content: ChatMessageContent::Text("Please analyze this image.".to_string()),
                name: Some("User_Query".to_string()),
                ..Default::default()
            },
            ChatMessage {
                role: Role::User,
                content: ChatMessageContent::ImageUrl(vec![ImageUrl {
                    r#type: "image_url".to_string(),
                    text: None,
                    image_url: ImageUrlType { url, detail: None },
                }]),
                name: Some("Image_URL".to_string()),
                ..Default::default()
            },
        ],
        max_tokens: Some(200),
        frequency_penalty: Some(0.0),
        ..Default::default()
    };

    match client.chat().create(parameters).await {
        Ok(result) => {
            if let Some(choice) = result.choices.get(0) {
                match &choice.message.content {
                    ChatMessageContent::Text(text) => {
                        let clear_text = trim_at_last_period(text);
                        HttpResponse::Ok().content_type("text/plain").body(clear_text.to_string())
                    },
                    _ => HttpResponse::BadRequest().body("Unsupported content type received"),
                }
            } else {
                HttpResponse::InternalServerError().body("No response content available.")
            }
        }
        Err(err) => HttpResponse::InternalServerError().body(format!("Failed to create chat completion: {:?}", err)),
    }
}

fn trim_at_last_period(text: &str) -> &str {
    if let Some(pos) = text.rfind('.') {
        &text[..=pos]
    } else {
        text
    }
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    dotenv().ok();
    let api_key = env::var("OPENAI_API_KEY").expect("$OPENAI_API_KEY is not set");
    let client = Client::new(api_key);

    HttpServer::new(move || {
        App::new()
            .data(client.clone())
            .route("/analyze", web::get().to(analyze_image))
            .service(actix_files::Files::new("/", "./static/").index_file("index.html"))
    })
    .bind("127.0.0.1:8085")?
    .run()
    .await
}
